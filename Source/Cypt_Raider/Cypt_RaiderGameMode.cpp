// Copyright Epic Games, Inc. All Rights Reserved.

#include "Cypt_RaiderGameMode.h"
#include "Cypt_RaiderCharacter.h"
#include "UObject/ConstructorHelpers.h"

ACypt_RaiderGameMode::ACypt_RaiderGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPerson/Blueprints/BP_FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

}
