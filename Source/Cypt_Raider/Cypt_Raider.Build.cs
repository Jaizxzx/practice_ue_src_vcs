// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Cypt_Raider : ModuleRules
{
	public Cypt_Raider(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "EnhancedInput" });
	}
}
