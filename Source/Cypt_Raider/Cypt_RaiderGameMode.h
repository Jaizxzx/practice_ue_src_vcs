// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Cypt_RaiderGameMode.generated.h"

UCLASS(minimalapi)
class ACypt_RaiderGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ACypt_RaiderGameMode();
};



